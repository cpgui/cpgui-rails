# cpgui-web

A web module for the cpgui.

\#WIP

## Installation

1. Install git: <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>
2. Install [rbenv](https://github.com/rbenv/rbenv) and install ruby
3. Install [cpgui](https://gitlab.com/cpgui/cpgui)
4. Download the repository with the command: `git clone https://github.com/cpgui/cpgui.git` to get the latest build or download the latest release here <https://gitlab.com/cpgui/cpgui-web/releases>
5. Move the folder in the modules directory from the cpgui.

## Features

* Customizable websites with erb
* Module support _(Add your module to the modules directory and you can use here)_
* Language suppport _(You can use this core feature on the website too!)_
* you can create your own websites which does not use a module!
